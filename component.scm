;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2022 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(##namespace ("ts--c#"
parent-scopes
workspace-configuration
imported-namespaces
hover-information
filter-completions
))

;; propagate the associated filetypes.
(set! languages-by-suffix
  (append languages-by-suffix
          '((".h" . "c")
            (".c" . "c"))))

;; add the own language-object of ts
(set! known-languages
  (cons (cons "c" (ts--c#language-new))
        known-languages))

(define parent-scopes
  '(("translation_unit" all)
    ; this aids writing within a function:
    ; first compound_statement for locals. If not found,
    ; query within the function.
    ("function_declarator" member)
    ("compound_statement" local)))

;; language dependend
(define keywords
  '("auto" "break" "case" "char" "const" "continue" "default"
    "do" "double" "else" "enum" "extern" "float" "for"
    "goto" "if" "inline" "int" "long" "register" "restrict"
    "return" "short" "signed" "sizeof" "static" "struct" "switch"
    "typedef" "union" "unsigned" "void" "volatile" "while" "_Alignas"
    "alignas" "_Alignof" "alignof" "_Atomic" "_Bool" "bool" "_Complex"
    "complex" "_Decimal128" "_Decimal32" "_Decimal64" "_Generic" "_Imaginary"
    "imaginary" "_Noreturn" "noreturn" "_Static_assert" "static_assert"
    "_Thread_local" "thread_local" "_Pragma" "if" "elif" "else" "endif"
    "ifdef" "ifndef" "define" "undef" "include" "line" "error" "pragma"
    "defined" "__has_c_attribute"))

(define (imported-namespaces uri doc)
  (let* ((ts--imports (read-file-relative-to (this-source-file) "queries/imports.ss"))
         (imports
           (fold (lambda (imp acc)
                   (let* ((raw (read-node (lsp-document-content doc) (cadr (member "name" imp))))
                          ;; remove leading quote or diamond, and drop when suffix starts.
                          ;; FIXME: includes can contain a leading directory, which is not handled yet.
                          (full (substring raw 1 (- (string-length raw) 3))))
                     (cons full acc)))
                 '("string") ;; standard libraries, which should be made accessible, should be listed here.
                 (ts--query-collect! doc ts--imports (ts--tree-root-node (lsp-document-tree doc))))))
    (fold (lambda (it acc) (if (member it acc) acc (cons it acc))) '() imports)))

(define (filter-completions item-vector isIncomplete-box cursor uri doc word current-node)
  (vector-append
    ;; append the keywords.
    (list->vector
      (fold
        (lambda (keyword acc)
          (if (>= (string-length keyword) strlen)
              (if (string=? (substring keyword 0 strlen) search-string)
                  (cons `(("label" . ,keyword)
                          ("kind" . ,completion-item-kind-keyword)
                          ("detail" . "C Syntax")) acc)
                  acc)
              acc))
        '()
        keywords))
    item-vector))
