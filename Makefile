# buildfile for the c language implementation of zamba.

GSC=gsc
GSC_OPTIONS=
CC_OPTIONS="-Wall "
LD_OPTIONS="-ltree-sitter -ltree-sitter-c"

OPTIONS= $(GSC_OPTIONS) -cc-options $(CC_OPTIONS) -ld-options $(LD_OPTIONS)

all:
	$(GSC) $(OPTIONS) -o $(BIN_DIR)/ts-c-binding.o1 ts-c-binding.scm
